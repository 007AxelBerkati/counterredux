import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {Provider} from 'react-redux';
import Counter from './src';
import storeState from './src/store';

const App = () => {
  return (
    <Provider store={storeState}>
      <Counter />
    </Provider>
  );
};

export default App;

const styles = StyleSheet.create({});
