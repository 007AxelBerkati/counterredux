import { applyMiddleware, createStore } from 'redux';
import reduxLogger from 'redux-logger';
import reducers from '../reducers';

const storeState = createStore(reducers, applyMiddleware(reduxLogger));
export default storeState;
