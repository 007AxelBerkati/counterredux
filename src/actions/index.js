import {TAMBAH, KURANG, BAGI, KALI} from '../type';

export const tambahinAngka = counter => ({
  type: TAMBAH,
  data: counter,
});

export const kuranginAngka = counter => ({
  type: KURANG,
  data: counter,
});

export const bagiAngka = counter => ({
  type: BAGI,
  data: counter,
});

export const kaliAngka = counter => ({
  type: KALI,
  data: counter,
});
