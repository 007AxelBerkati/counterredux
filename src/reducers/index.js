import {TAMBAH, KURANG, KALI, BAGI} from '../type';

const initialState = {
  angkaSekarang: 0,
};

const reducers = (state = initialState, action) => {
  switch (action.type) {
    case TAMBAH:
      console.log('TAMBAH COUNTER');
      return {...state, angkaSekarang: action.data + 1};
    case KURANG:
      console.log('KURANG COUNTER');
      return {...state, angkaSekarang: action.data - 1};
    case KALI:
      console.log('KALI COUNTER');
      return {...state, angkaSekarang: action.data * 2};
    case BAGI:
      console.log('BAGI COUNTER');
      return {...state, angkaSekarang: action.data / 2};
    default:
      return state;
  }
};

export default reducers;
