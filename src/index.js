import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {
  kuranginAngka,
  tambahinAngka,
  bagiAngka,
  kaliAngka,
} from './actions/index';

const Counter = () => {
  const dispatch = useDispatch();
  const counter = useSelector(data => data.angkaSekarang);
  return (
    <View style={styles.page}>
      <TouchableOpacity
        style={styles.kurang}
        onPress={() => dispatch(kuranginAngka(counter))}>
        <Text style={styles.textKurang}>(-1)</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.tambah}
        onPress={() => dispatch(tambahinAngka(counter))}>
        <Text style={styles.textTambah}>(+1)</Text>
      </TouchableOpacity>
      <Text style={styles.counter}>{counter}</Text>
      <TouchableOpacity
        style={styles.kali}
        onPress={() => dispatch(kaliAngka(counter))}>
        <Text style={styles.textTambah}>(*2)</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.bagi}
        onPress={() => dispatch(bagiAngka(counter))}>
        <Text style={styles.textTambah}>(:2)</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Counter;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: 'white',
  },

  kurang: {
    backgroundColor: '#DDDDDD',
    paddingHorizontal: 20,
    paddingVertical: 5,
    marginRight: 20,
  },

  tambah: {
    backgroundColor: '#DDDDDD',
    paddingHorizontal: 20,
    paddingVertical: 5,
  },
  counter: {
    fontSize: 50,
    marginHorizontal: 20,
  },

  textKurang: {
    fontSize: 20,
  },
  textTambah: {
    fontSize: 20,
  },
  kali: {
    backgroundColor: '#DDDDDD',
    paddingHorizontal: 20,
    paddingVertical: 5,
    marginRight: 20,
  },
  bagi: {
    backgroundColor: '#DDDDDD',
    paddingHorizontal: 20,
    paddingVertical: 5,
  },
});
